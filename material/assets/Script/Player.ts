import score from "./score";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Player extends cc.Component 
{

    private anim = null; //this will use to get animation component

    private animateState = null; //this will use to record animationState

    @property(cc.Prefab)
    private bulletPrefab: cc.Prefab = null;

    @property()
    private maxBulletNum: number = 5;

    @property(score)
    private score: score = null;

    private bulletPool = null; // this is a bullet manager, and it control the bullet resource

    private playerSpeed: number = 0;

    private zDown: boolean = false; // key for player to go left

    private xDown: boolean = false; // key for player to go right

    private jDown: boolean = false; // key for player to shoot

    private kDown: boolean = false; // key for player to jump

    private onGround: boolean = false;

    private isDead: boolean = true;

    onLoad()
    {
        this.anim = this.getComponent(cc.Animation);

        cc.director.getCollisionManager().enabled = true;

        cc.director.getPhysicsManager().enabled = true;

        this.bulletPool = new cc.NodePool('Bullet');

        for(let i: number = 0; i < this.maxBulletNum; i++)
        {
            let bullet = cc.instantiate(this.bulletPrefab);

            this.bulletPool.put(bullet);
        }
    }

    start() 
    {
        // add key down and key up event
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }

    update(dt)
    {
        this.playerMovement(dt);

        this.playerAnimation();
    }

    onKeyDown(event) 
    {
        switch(event.keyCode) 
        {
            case cc.macro.KEY.z:

                this.zDown = true;

                this.xDown = false;

                break;

            case cc.macro.KEY.x:

                this.xDown = true;

                this.zDown = false;

                break;

            case cc.macro.KEY.j:

                this.jDown = true;

                break;

            case cc.macro.KEY.k:

                this.kDown = true;

                break;
        }
    }

    onKeyUp(event)
    {
        switch(event.keyCode) 
        {
            case cc.macro.KEY.z:

                this.zDown = false;

                break;

            case cc.macro.KEY.x:

                this.xDown = false;

                break;

            case cc.macro.KEY.j:

                this.jDown = false;

                break;

            case cc.macro.KEY.k:

                this.kDown = false;

                break;
        }
    }

    private playerMovement(dt)
    {
        if(this.isDead)
            this.playerSpeed = 0;
        else if(this.jDown || this.anim.getAnimationState('shoot').isPlaying)
            this.playerSpeed = 0;
        else if(this.zDown)
            this.playerSpeed = -300;
        else if(this.xDown)
            this.playerSpeed = 300;
        else
            this.playerSpeed = 0;

        this.node.x += this.playerSpeed * dt;  //move player
    }

    private playerAnimation()
    {
        this.node.scaleX = (this.zDown) ? -1 : (this.xDown) ? 1 : this.node.scaleX;

        if(this.isDead)
        {
            if(this.animateState == null || this.animateState.name != 'reborn')
            {
                //reset player position and play reborn animation
                this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);

                this.node.position = cc.v2(-192, 255);
                
                this.animateState = this.anim.play('reborn');

                this.anim.once('finished', (() => {
                    this.isDead = false;
                }));

                //reset score value
                this.score.resetScore();
            }
        }
        else if(!this.anim.getAnimationState('shoot').isPlaying && !this.anim.getAnimationState('jump').isPlaying && this.onGround) // move animation can play only when shoot or jump animation finished
        {
            if(this.jDown)
                this.animateState = this.anim.play('shoot');
            else if(this.kDown)
            {
                    this.animateState = this.anim.play('jump');

                    this.jump();
            }
            else if(this.zDown || this.xDown)
            {
                if(this.animateState == null || this.animateState.name != 'move') // when first call or last animation is not move
                    this.animateState = this.anim.play('move');
            }
            else
            {
                //if no key is pressed and the player is on ground, stop all animations and go back to idle
                if(this.animateState == null || this.animateState.name != 'idle')
                    this.animateState = this.anim.play('idle');
            }
        }
    }

    private jump()
    {
        this.onGround = false;

        this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 1500); //give velocity to the player
    }

    // call this when player shoots the bullet.
    private createBullet()
    {
        let bullet = null;

        if (this.bulletPool.size() > 0) 
            bullet = this.bulletPool.get(this.bulletPool);

        if(bullet != null)
            bullet.getComponent('Bullet').init(this.node);
    }

    //check if the collision is valid or not
    onBeginContact(contact, selfCollider, otherCollider)
    {
        if(otherCollider.tag == 1) //platform tag
        {
            this.onGround = true;

            if(this.anim.getAnimationState('jump').isPlaying)
                this.animateState  = this.anim.play('idle');
        }
        else if(otherCollider.tag == 2 && !this.isDead) //enemy tag
            this.isDead = true;
    }
}