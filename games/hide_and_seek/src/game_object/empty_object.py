import pygame.sprite

class EmptyObject(pygame.sprite.Sprite):
    def __init__(self, game):
        pygame.sprite.Sprite.__init__(self)
        self.game = game

    def update(self):
        pass

    @property
    def render_dict(self):
        return {}
