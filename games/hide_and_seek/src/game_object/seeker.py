import pygame.sprite
import math

from .ridar import Ridar

class Seeker(pygame.sprite.Sprite):
    def __init__(self, game, pos, name):
        pygame.sprite.Sprite.__init__(self)
        self.game = game
        self.isHider =  False
        self.name = name

        self.speed = self.game.config["seeker_speed"]

        self.ridars = []
        fov = self.game.config["ridar_fov"]
        density = self.game.config["ridar_density"]
        for a in range(-int(fov/2), int(fov/2), int(density)):
            self.ridars.append(Ridar(game, self, a / 180 * math.pi))

        self.image = pygame.Surface([30, 30])
        self.color = "#FF3333"
        self.rect = self.image.get_rect(center=pos)

        self.lastMotion = None

        self.angle = 0

        self.isHit = False
        self.isDead = False

    def update(self, motions):
        if self.isHit == False:
            for motion in motions:
                if motion == "LEFTROTATE":
                    self.angle = (self.angle + 0.1) % (2 * math.pi)
                elif motion == "RIGHTROTATE":
                    self.angle = (self.angle - 0.1) % (2 * math.pi)
                elif motion == "FORWARD":
                    radians = self.angle - math.radians(90)
                    self.lastMotion = [-1 * math.cos(radians) * self.speed, math.sin(radians) * self.speed]
                    
                    self.rect.centerx += self.lastMotion[0]
                    self.rect.centery += self.lastMotion[1]
                
                elif motion == "BACKWARD":
                    radians = self.angle - math.radians(90)
                    self.lastMotion = [math.cos(radians) * self.speed, -1 * math.sin(radians) * self.speed]
                    
                    self.rect.centerx += self.lastMotion[0]
                    self.rect.centery += self.lastMotion[1]
                    
        for ridar in self.ridars:
            ridar.update()
        self.isHit = False

    def recoverWallColl(self, hits):
        self.isHit = True

        if len(hits) == 1:
            
            if hits[0].rect.centery <= self.rect.centery - 15:
                self.rect.centery = hits[0].rect.centery + 45
            elif hits[0].rect.centery >= self.rect.centery + 15:
                self.rect.centery = hits[0].rect.centery - 45
            if hits[0].rect.centerx <= self.rect.centerx - 15:
                self.rect.centerx = hits[0].rect.centerx + 35
            elif hits[0].rect.centerx >= self.rect.centerx + 15:
                self.rect.centerx = hits[0].rect.centerx - 35
        else:
            blockDir = [0, 0, 0, 0] # top, down, left, right
            average_x = 0
            average_y = 0
            for hit in hits:
                if hit.rect.centery <= self.rect.centery - self.speed:
                    average_y += hit.rect.centery
                    blockDir[0] += 1
                if hit.rect.centery >= self.rect.centery + self.speed:
                    average_y += hit.rect.centery
                    blockDir[1] += 1
                if hit.rect.centerx <= self.rect.centerx - self.speed:
                    average_x += hit.rect.centerx
                    blockDir[2] += 1
                if hit.rect.centerx >= self.rect.centerx + self.speed:
                    average_x += hit.rect.centerx
                    blockDir[3] += 1
            
            

            isSameDir = False
            for dir in blockDir:
                if dir == len(hits):
                    isSameDir = True
                    average_x = average_x / len(hits)
                    average_y = average_y / len(hits)
                    if blockDir.index(dir) == 0:
                        # self.rect.centery = hits[0].rect.centery + 45
                        self.rect.centery = average_y + 45
                    elif blockDir.index(dir) == 1:
                        self.rect.centery = average_y - 45
                    elif blockDir.index(dir) == 2:
                        self.rect.centerx = average_x + 35
                    elif blockDir.index(dir) == 3:
                        self.rect.centerx = average_x - 35

            if isSameDir:
                return
            else:
                print(blockDir)
                for dir in blockDir:
                    if dir == max(blockDir):
                        if blockDir.index(dir) == 0:
                            self.rect.centery = hits[0].rect.centery + 45
                        elif blockDir.index(dir) == 1:
                            self.rect.centery = hits[0].rect.centery - 45
                        if blockDir.index(dir) == 2:
                            self.rect.centerx = hits[0].rect.centerx + 35
                        elif blockDir.index(dir) == 3:
                            self.rect.centerx = hits[0].rect.centerx - 35

    @property
    def render_dicts(self):
        render_dicts = []
        render_dicts.append({
                "type": "image",
                "name": "ball",
                "x": self.rect.centerx,
                "y": self.rect.centery,
                "angle": self.angle,
                "width": self.rect.width,
                "height": self.rect.height,
                "image_id": "seeker"
        })

        for ridar in self.ridars:
            render_dicts.append(ridar.render_dict)
            
        return render_dicts
    
    @property
    def info(self):
        status = "seeking"
    
        return {
            "name": "seeker",
            "x": self.rect.centerx,
            "y": self.rect.centery,
            "angle": self.angle,
            "status": status
        }

    @property
    def status(self):
        return "seeking"       

    @property
    def ridars_info(self):
        return [ ridar.info for ridar in self.ridars]
