import pygame.sprite
from ..physic import *
import math

class Ridar(pygame.sprite.Sprite):
    def __init__(self, game, player, angle_offset):
        pygame.sprite.Sprite.__init__(self)
        self.game = game
        self.player = player
        self.angle_offset = angle_offset
        self.color = "#ffffff"
        self.collode_point = None
        self.collide_obj = None
        self.distance = 0

    def raycast(self):
        start = self.player.rect.center
        look_dir = angle_vec(self.player.angle + self.angle_offset)
        end = start + look_dir * self.game.config["ridar_max_distance"]

        self.color = "#ffffff"
        collide_obj, min_dis, collode_point = None, math.inf, end

        for wall in self.game.map.sprite_group.sprites():
            collide_p = rect_collideline(wall.rect, (start, end))
            if collide_p:
                distance = (start - collide_p).length()
                if distance < min_dis:
                    self.color = "#dcff2b"
                    min_dis = distance
                    collode_point = collide_p
                    collide_obj = wall

        for player in self.game.players.values():
            if player == self.player:
                continue
            if player.status == "dead":
                continue

            collide_p = rect_collideline(player.rect, (start, end))
            if not collide_p:
                continue
            
            distance = (start - collide_p).length()
            if distance < min_dis:
                self.color = "#FF3333"
                min_dis = distance
                collode_point = collide_p
                collide_obj = player 

        self.collode_point = collode_point
        self.collide_obj = collide_obj
        self.distance = min_dis

    def update(self):
        self.raycast()

    @property
    def render_dict(self):
        return {"type": "line",
                "name": "ray",
                "x1": self.player.rect.centerx,
                "y1": self.player.rect.centery,
                "x2": self.collode_point[0],
                "y2": self.collode_point[1],
                "width": 2,
                "color": self.color
            }

    @property
    def info(self):
        if not self.collide_obj:
            return {
                "obj": None,
                "distance": math.inf,
                "angle_offset": self.angle_offset
            }
        return {
            "obj": self.collide_obj.info,
            "distance": self.distance,
            "angle_offset": self.angle_offset
        }