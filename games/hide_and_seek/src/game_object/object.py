import pygame.sprite


class Wall(pygame.sprite.Sprite):
    def __init__(self, group, game, position):
        pygame.sprite.Sprite.__init__(self, group)
        self.game = game

        self.image = pygame.Surface([40, 60])
        self.color = "#FFFFFF"
        self.rect = self.image.get_rect()
        self.rect.center = (position[1] * 40 + 20, position[0] * 60 + 30)

    def update(self):
        pass

    @property
    def render_dict(self):
        return {"type": "rect",
                "name": "wall",
                "x": self.rect.x,
                "y": self.rect.y,
                "angle": 0,
                "width": self.rect.width,
                "height": self.rect.height,
                "color": self.color
                }

    @property
    def info(self):
        return {
            "name": "wall",
            "x": self.rect.centerx,
            "y": self.rect.centery,
            "angle": 0,
            "status": None,
        }
