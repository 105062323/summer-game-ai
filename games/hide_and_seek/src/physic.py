import math
from pygame import Rect
from pygame.math import Vector2

def line_intersect(line_a, line_b):
    """
    Check two line segments intersect

    @param line_a A tuple (Vector2, Vector2) representing both end points
           of line segment
    @param line_b Same as `line_a`

    """
    # line_a and line_b have the same end point
    if (line_a[0] == line_b[0] or
        line_a[0] == line_b[1]):
        return line_a[0]
    if (line_a[1] == line_b[0] or
        line_a[1] == line_b[1]):
        return line_a[1]

    # Set line_a to (u0, u0 + v0) and p0 = u0 + s * v0, and
    # set line_b to (u1, u1 + v1) and p1 = u1 + t * v1,
    # where u, v, p are vectors and s, t is in [0, 1].
    # If line_a and line_b intersects, then p0 = p1
    # -> u0 - u1 = -s * v0 + t * v1
    # -> | u0.x - u1.x |   | v0.x  v1.x | |-s |
    #    | u0.y - u1.y | = | v0.y  v1.y | | t |
    #
    # If left-hand vector is a zero vector, then two line segments has the same end point.
    # If the right-hand matrix is not invertible, then two line segments are parallel.
    # If none of above conditions is    , find the solution of s and t,
    # if both s and t are in [0, 1], then two line segments intersect.

    v0 = line_a[1] - line_a[0]
    v1 = line_b[1] - line_b[0]
    det = v0.x * v1.y - v0.y * v1.x
    # Two line segments are parallel
    if det == 0:
        # TODO Determine if two lines overlap
        return None

    du = line_a[0] - line_b[0]
    s_det = v1.x * du.y - v1.y * du.x
    t_det = v0.x * du.y - v0.y * du.x

    # if det > 0 and 0 <= s_det <= det and 0 <= t_det <= det:

    if ((det > 0 and 0 <= s_det <= det and 0 <= t_det <= det) or
        (det < 0 and det <= s_det <= 0 and det <= t_det <= 0)):
        return line_a[0] + s_det / det * v0

    return None

def angle_vec(angle) -> Vector2:
    radians = angle - math.radians(90)
    look_dir = Vector2(-1 * math.cos(radians), math.sin(radians))
    return look_dir

def rect_collideline(rect: Rect, line):
    """
    Check line segment intersects with a rect and return point

    @param rect The Rect of the target rectangle
    @param line A tuple (Vector2, Vector2) representing both end points
           of line segment
    """
    # Either of line ends is in the target rect.

    line_top = (Vector2(rect.topleft), Vector2(rect.topright))
    line_bottom = (Vector2(rect.bottomleft), Vector2(rect.bottomright))
    line_left = (Vector2(rect.topleft), Vector2(rect.bottomleft))
    line_right = (Vector2(rect.topright), Vector2(rect.bottomright))

    for rect_line in [line_top, line_bottom, line_left, line_right]:
        inter_point = line_intersect(rect_line, line)
        if inter_point:
            return inter_point

    return None