from games.hide_and_seek.src.game_object.object import Wall
import pygame
import time

class Map:
    def __init__(self, game, settings):
        self._begin_time = time.time()
        self._line_time = 0
        
        self.game = game
        
        self.sprite_group = pygame.sprite.Group()
        self.player_sprite_group = pygame.sprite.Group()

        for line_index in range(len(settings)):
            for block_index in range(len(settings[line_index])):
                if settings[line_index][block_index] == 1:
                    Wall(self.sprite_group, game, [line_index, block_index])

    def update(self):
        for player in self.game.players:
            hits = pygame.sprite.spritecollide(self.game.players[player], self.sprite_group, False, pygame.sprite.collide_rect_ratio(1.0))
            if hits:
                self.game.players[player].recoverWallColl(hits)
        
        for player in self.game.players:
            self.game.players[player].attachHider = []
            for another_player in self.game.players:
                if player != another_player:
                    self.player_sprite_group = pygame.sprite.Group()
                    self.player_sprite_group.add(self.game.players[another_player])
                    hits = pygame.sprite.spritecollide(self.game.players[player], self.player_sprite_group , False, pygame.sprite.collide_rect_ratio(1.0))
                    if hits:
                        if self.game.players[player].isHider == True and self.game.players[another_player].isHider == True:
                            self.game.players[player].attachHider.append(self.game.players[another_player])
                        else:
                            if self.game.players[player].isHider == True and self.game.players[player].isLock == False:
                                self.game.players[player].isDead = True
                            if self.game.players[another_player].isHider == True and self.game.players[another_player].isLock == False:
                                self.game.players[another_player].isDead = True
                        # 

    @property
    def render_dicts(self):
        objects_data = []
        for wall in self.sprite_group:
            objects_data.append(wall.render_dict)
        return objects_data

    @property
    def objects_info(self):
        objects_info = []
        for wall in self.sprite_group:
            objects_info.append(wall.info)
        return objects_info