
import pygame
from os import path
import math

from mlgame.gamedev.game_interface import PaiaGame, GameResultState, GameStatus
from mlgame.view.test_decorator import check_game_progress, check_game_result
from mlgame.view.view_model import create_text_view_data, create_asset_init_data, create_image_view_data, Scene

from .game_object.hider import Hider
from .game_object.seeker import Seeker
from .scene import Map
from .mapConfig import mapConfig

ASSET_PATH = path.join(path.dirname(__file__), "../asset")


class HideAndSeek(PaiaGame):
    """
    This is a Interface of a game
    """

    def __init__(self,
        seeker_speed,
        hider_speed,
        # ridar
        ridar_fov, 
        ridar_density, 
        ridar_max_distance, 
        ridar_visible,
        game_time
    ):
        super().__init__()
        self.config = {
            "seeker_speed":seeker_speed,
            "hider_speed":hider_speed,
            "ridar_fov":ridar_fov,
            "ridar_density":ridar_density,
            "ridar_max_distance":ridar_max_distance,
            "ridar_visible": ridar_visible,
            "game_time": game_time
        }

        self.scene = Scene(width=800, height=600,
                           color="#4FC3F7", bias_x=0, bias_y=0)
        self.frame_count = 0
        self.frame_count_to_win = self.config["game_time"]

        self.map = Map(self, mapConfig)
        # game_object
        self.players = {
            "seeker1": Seeker(self, (100, 100), "seeker1"),
            "hider1": Hider(self, (700, 500), "hider1"),
            "hider2": Hider(self, (700, 100), "hider2"),
            "hider3": Hider(self, (400, 300), "hider3"),
        }

    def update(self, commands):
        self.map.update()
        # handle command
        for name, object in self.players.items():
            cmd = commands[name]
            object.update(cmd)

        self.frame_count += 1
        if not self.is_running:
            return "QUIT"

    def game_to_player_data(self):
        """
        send something to game AI
        we could send different data to different ai
        """
        to_players_data = {}

        # hider
        hiders_info = [
            self.players["hider1"].info, 
            self.players["hider2"].info, 
            self.players["hider3"].info
        ]


        def collect_data(player_name, teammates):
            to_data = self.players[player_name].info
            to_data['ridars'] = self.players[player_name].ridars_info
            to_data["teammates"] = teammates
            return to_data


        to_players_data["hider1"] = collect_data("hider1", hiders_info)
        to_players_data["hider2"] = collect_data("hider2", hiders_info)
        to_players_data["hider3"] = collect_data("hider3", hiders_info)
        
        to_players_data["seeker1"] = collect_data("seeker1", [])
        return to_players_data

    def reset(self):
        pass

    def get_game_status(self):

        if self.is_running:
            status = GameStatus.GAME_ALIVE
        elif self.frame_count > self.frame_count_to_win:
            status = GameStatus.GAME_PASS
        else:
            status = GameStatus.GAME_OVER
        return status

    @property
    def is_running(self):
        isAllHiderDead = False
        isRemindOneHiderandLock = False
        isRemindHiderAllLock = True

        remindList = []
        for player in self.players:
            if self.players[player].isHider == True:
                if self.players[player].isDead == False:
                    remindList.append(player)
        
        if len(remindList) == 0:
            isAllHiderDead = True
        elif len(remindList) == 1:
            if self.players[remindList[0]].isLock == True:
                isRemindOneHiderandLock = True
        else:
            for player in remindList:
                if self.players[player].isLock == False:
                    isRemindHiderAllLock = False

        return isAllHiderDead == False and isRemindOneHiderandLock == False and isRemindHiderAllLock == False

    def get_scene_init_data(self):
        """
        Get the initial scene and object information for drawing on the web
        """
        # add music or sound
        bg_path = path.join(ASSET_PATH, "img/background.jpg")
        background = create_asset_init_data(
            "background", 800, 600, bg_path, "url")

        player_path = path.join(ASSET_PATH, "img/tank1.png")
        player = create_asset_init_data("player", 600, 600, player_path, "url")

        seeker_path = path.join(ASSET_PATH, "img/tank2.png")
        seeker = create_asset_init_data("seeker", 600, 600, seeker_path, "url")

        scene_init_data = {"scene": self.scene.__dict__,
                           "assets": [
                               background, player, seeker
                           ],
                           # "audios": {}
                           }
        return scene_init_data

    @check_game_progress
    def get_scene_progress_data(self):
        """
        Get the position of game objects for drawing on the web
        """
        game_obj_list = []
        for player_object in self.players.values():
            game_obj_list += player_object.render_dicts
        game_obj_list.extend(self.map.render_dicts)

        background = create_image_view_data("background", 400, 300, 800, 600)

        timer_text = create_text_view_data(
            "Frame: " + str(self.frame_count), 650, 50, "#FFAA00")
        scene_progress = {
            # background view data will be draw first
            "background": [
                background,
            ],
            # game object view data will be draw on screen by order , and it could be shifted by WASD
            "object_list": game_obj_list,
            "toggle": [timer_text],
            "foreground": [],
            # other information to display on web
            "user_info": [],
            # other information to display on web
            "game_sys_info": {}
        }
        return scene_progress

    @check_game_result
    def get_game_result(self):
        """
        send game result
        """
        if self.get_game_status() == GameStatus.GAME_PASS:
            self.game_result_state = GameResultState.FINISH

        attachment = []
        for player in self.ai_clients():
            name = player["name"]
            player_data = self.players[name]
            attachment.append({"player": name,
                               "status": player_data.status
                               })

        return {"frame_used": self.frame_count,
                "state": self.game_result_state,
                "attachment": attachment}

    def get_keyboard_command(self):
        """
        Define how your game will run by your keyboard
        """
        return {
            "seeker1": key2motion(pygame.K_UP, pygame.K_DOWN, pygame.K_LEFT, pygame.K_RIGHT),
            "hider1": key2motion(pygame.K_t, pygame.K_g, pygame.K_f, pygame.K_h, pygame.K_r, pygame.K_y),
            "hider2": key2motion(pygame.K_KP8, pygame.K_KP5, pygame.K_KP4, pygame.K_KP6, pygame.K_KP7, pygame.K_KP9),
            "hider3": key2motion(pygame.K_w, pygame.K_s, pygame.K_a, pygame.K_d, pygame.K_q, pygame.K_e),
        }

    @staticmethod
    def ai_clients():
        """
        let MLGame know how to parse your ai,
        you can also use this names to get different cmd and send different data to each ai client
        """
        return [
            {"name": "seeker1"},
            {"name": "hider1"},
            {"name": "hider2"},
            {"name": "hider3"},
        ]


def key2motion(up_key, down_key, left_key, right_key, lock_key=None, unlock_key=None):
    cmd = []
    key_pressed_list = pygame.key.get_pressed()
    if key_pressed_list[up_key]:
        cmd.append("FORWARD")
    if key_pressed_list[down_key]:
        cmd.append("BACKWARD")
    if key_pressed_list[left_key]:
        cmd.append("LEFTROTATE")
    if key_pressed_list[right_key]:
        cmd.append("RIGHTROTATE")
    if lock_key is not None:
        if key_pressed_list[lock_key]:
            cmd.append("LOCK")
    if unlock_key is not None:
        if key_pressed_list[unlock_key]:
            cmd.append("UNLOCK")
    return cmd
