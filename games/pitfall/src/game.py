import time
import pygame

from mlgame.gamedev.game_interface import PaiaGame
from mlgame.view.test_decorator import check_game_progress
from mlgame.view.view_model import create_text_view_data, create_asset_init_data, create_image_view_data, Scene
from .player import Player
from .scene import SceneManager
from .treasure import TreasureManager
from os import path

ASSET_PATH = path.join(path.dirname(__file__), "../asset")


class PitFall(PaiaGame):
    """
    This is a Interface of a game
    """

    def __init__(self, difficulty):
        super().__init__()
        self.scene = Scene(width=800, height=600,
                           color="#4FC3F7", bias_x=0, bias_y=0)
        self.treasureManager = TreasureManager()
        self.sceneManager = SceneManager(self)
        self.player = Player()

        self._begin_time = time.time()
        self._timer = 0
        self.frame_count = 0

    def update(self, commands):
        # handle command
        ai_1p_cmd = commands[self.ai_clients()[0]["name"]]

        if self.player.rect.centerx > 770:
            status = self.sceneManager.change_level(True)
            if status:
                self.player.rect.centerx = 30
        elif self.player.rect.centerx < 30:
            status = self.sceneManager.change_level(False)
            if status:
                self.player.rect.centerx = 770
        self.player.update(ai_1p_cmd)
        self.sceneManager.scene.update()

        self._timer = round(time.time() - self._begin_time, 3)

        self.frame_count += 1

        if not self.is_running:
            return "QUIT"

    def game_to_player_data(self):
        """
        send something to game AI
        we could send different data to different ai
        """
        to_players_data = {}
        # TODO: Send some data to player
        # treasures_data = []
        # for treasure in self.treasures:
        #     treasures_data.append({"x": treasure.rect.x, "y": treasure.rect.y})
        # data_to_1p = {
        #     "ball_x": self.player.rect.centerx,
        #     "ball_y": self.player.rect.centery,
        #     "foods": treasures_data,
        #     "score": self.score
        # }
        # sceneObjs = self.sceneManager.game_object_datas
        data_to_1p = {
            "level": self.sceneManager.level,
            "player_x": self.player.x,
            "player_y": self.player.y,
            "score": self.treasureManager.now_collect,
            "objects": [self.sceneManager.scene.objects_info],
        }

        for ai_client in self.ai_clients():
            to_players_data[ai_client['name']] = data_to_1p
        # should be equal to config. GAME_SETUP["ml_clients"][0]["name"]

        return to_players_data

    def reset(self):
        pass

    @property
    def is_running(self):
        # return self.frame_count < 300
        return not self.treasureManager.is_collect_all()

    def get_scene_init_data(self):
        """
        Get the initial scene and object information for drawing on the web
        """
        # add music or sound
        bg_path = path.join(ASSET_PATH, "img/background.png")
        background = create_asset_init_data(
            "background", 800, 600, bg_path, "url")

        ground_path = path.join(ASSET_PATH, "img/ground.png")
        ground = create_asset_init_data("ground", 800, 600, ground_path, "url")

        log_path = path.join(ASSET_PATH, "img/log32.png")
        log = create_asset_init_data("log", 32, 32, log_path, "url")

        rolling_log_path = path.join(ASSET_PATH, "img/log32.png")
        rolling_log = create_asset_init_data(
            "rolling_log", 32, 32, rolling_log_path, "url")

        crocodile_close_path = path.join(ASSET_PATH, "img/crocodile32.png")
        crocodile_close = create_asset_init_data("crocodile_close", 32, 32, crocodile_close_path, "url")

        crocodile_open_path = path.join(ASSET_PATH, "img/crocodile_o.png")
        crocodile_open = create_asset_init_data("crocodile_open", 32, 32, crocodile_open_path, "url")

        treasure_path = path.join(ASSET_PATH, "img/treasure.png")
        treasure = create_asset_init_data(
            "treasure", 48, 48, treasure_path, "url")

        player_R1_path = path.join(ASSET_PATH, "img/player_R1.png")
        player_R1 = create_asset_init_data(
            "player_R1", 48, 48, player_R1_path, "url")

        player_R2_path = path.join(ASSET_PATH, "img/player_R2.png")
        player_R2 = create_asset_init_data(
            "player_R2", 48, 48, player_R2_path, "url")

        player_R3_path = path.join(ASSET_PATH, "img/player_R3.png")
        player_R3 = create_asset_init_data(
            "player_R3", 48, 48, player_R3_path, "url")

        player_L1_path = path.join(ASSET_PATH, "img/player_L1.png")
        player_L1 = create_asset_init_data(
            "player_L1", 48, 48, player_L1_path, "url")

        player_L2_path = path.join(ASSET_PATH, "img/player_L2.png")
        player_L2 = create_asset_init_data(
            "player_L2", 48, 48, player_L2_path, "url")

        player_L3_path = path.join(ASSET_PATH, "img/player_L3.png")
        player_L3 = create_asset_init_data(
            "player_L3", 48, 48, player_L3_path, "url")

        water_path = path.join(ASSET_PATH, "img/water.png")
        water = create_asset_init_data(
            "water", 150, 20, water_path, "url")

        crocodile_path = path.join(ASSET_PATH, "img/crocodile32.png")
        crocodile_close = create_asset_init_data("crocodile_close", 64, 64, crocodile_path, "url")

        crocodile_open_path = path.join(ASSET_PATH, "img/crocodile_o.png")
        crocodile_open = create_asset_init_data("crocodile_open", 64, 64, crocodile_open_path, "url")

        log1_path = path.join(ASSET_PATH, "img/log32.png")
        log1 = create_asset_init_data("log1", 64, 64, log1_path, "url")

        log2_path = path.join(ASSET_PATH, "img/logR32.png")
        log2 = create_asset_init_data("log2", 64, 64, log2_path, "url")


        scene_init_data = {"scene": self.scene.__dict__,
                           "assets": [
                               background, ground,water,
                               log, rolling_log, treasure,crocodile_close,crocodile_open,player_R1, player_R2, player_R3, player_L1, player_L2, player_L3, log1,log2
                           ],
                           # "audios": {}
                           }
        return scene_init_data

    @check_game_progress
    def get_scene_progress_data(self):
        """
        Get the position of game objects for drawing on the web
        """

        # add player, scene
        game_obj_list = [self.player.render_data]
        game_obj_list.extend(self.sceneManager.render_datas)

        # add ui, background
        background = create_image_view_data("background", 0, 0, 800, 600)
        ground = create_image_view_data("ground", 0, 0, 800, 600)
        score_text = create_text_view_data(
            "Score = " + str(self.treasureManager.now_collect), 650, 50, "#FF0000")
        timer_text = create_text_view_data(
            "Timer = " + str(self._timer) + " s", 650, 100, "#FFAA00")
        scene_progress = {
            # background view data will be draw first
            "background": [
                background,
                ground
                
            ],
            # game object view data will be draw on screen by order , and it could be shifted by WASD
            "object_list": game_obj_list,
            "toggle": [timer_text],
            "foreground": [
                score_text,
            ],
            # other information to display on web
            "user_info": [],
            # other information to display on web
            "game_sys_info": {}
        }
        return scene_progress

    def get_game_result(self):
        """
        send game result
        """
        return {"frame_used": self.frame_count}

    def get_keyboard_command(self):
        """
        Define how your game will run by your keyboard
        """
        cmd_1p = []
        key_pressed_list = pygame.key.get_pressed()

        if key_pressed_list[pygame.K_SPACE]:
            cmd_1p.append("SPACE")

        if key_pressed_list[pygame.K_LEFT]:
            cmd_1p.append("LEFT")
        if key_pressed_list[pygame.K_RIGHT]:
            cmd_1p.append("RIGHT")

        if key_pressed_list[pygame.K_q]:
            self.sceneManager.change_level(False)
        if key_pressed_list[pygame.K_e]:
            self.sceneManager.change_level(True)
        ai_1p = self.ai_clients()[0]["name"]
        return {ai_1p: cmd_1p}

    @staticmethod
    def ai_clients():
        """
        let MLGame know how to parse your ai,
        you can also use this names to get different cmd and send different data to each ai client
        """
        return [
            {"name": "1P"}
        ]
