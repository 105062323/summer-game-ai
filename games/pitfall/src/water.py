import pygame.sprite
from .object import ObjectType

from mlgame.view.view_model import create_image_view_data

class Water(pygame.sprite.Sprite):
    def __init__(self, group):
        pygame.sprite.Sprite.__init__(self, group)
        self.width = 120
        self.height = 20
        self.image = create_image_view_data("water", 340, 455, self.width, self.height)
        self.rect = pygame.Rect(340, 455, self.width, self.height)
        # self.image = pygame.Surface([150, 10])
        # self.color = "#28FFF4"
        # self.rect = self.image.get_rect()
        # self.rect.centerx = 400
        # self.rect.centery = 472
        # self.angle = 0

    @property
    def render_data(self):
        return self.image

    @property
    def info(self):
        return {
            "type": str(ObjectType.WATER),
            "x": self.rect.x,
            "y": self.rect.y,
            "width": self.rect.width,
            "height": self.rect.height,
            "status": 0
        }