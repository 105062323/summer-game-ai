import pygame.sprite
from .object import ObjectType

class Line(pygame.sprite.Sprite):
    def __init__(self, group):
        pygame.sprite.Sprite.__init__(self, group)
        self.data = [400, 150, 190, 360]
        
        self.width = 2
        self.color = "#FFFFFF"
        self.state = 0 # 0~4
        self.dir = 0

    def update(self):
        if self.dir == 0:
            self.state += 1
            if self.state == 4:
                self.dir = 1
        else:
            self.state -= 1
            if self.state == 0:
                self.dir = 0
        
        if self.state == 0:
            self.data = [400, 150, 224, 326]
        elif self.state == 1:
            self.data = [400, 150, 275, 366]
        elif self.state == 2:
            self.data = [400, 150, 400, 400]
        elif self.state == 3:
            self.data = [400, 150, 525, 366]
        else:
            self.data = [400, 150, 576, 326]


    @property
    def render_data(self):
        return {
            "type": "line",
            "name": "Line",
            "x1": self.data[0],
            "y1": self.data[1],
            "x2": self.data[2],
            "y2": self.data[3],
            "width": self.width,
            "color": self.color
        }


    @property
    def info(self):
        return {
            "type": str(ObjectType.VINE),
            "x": self.data[2],
            "y": self.data[3],
            "width": 0,
            "height": 0,
            "status": self.state
        }