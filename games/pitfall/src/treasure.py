import pygame.sprite
from .object import ObjectType
from mlgame.view.view_model import create_image_view_data

class TreasureManager:
    def __init__(self):
        self.now_collect = 0
        self.total = 5

    def collect(self):
        self.now_collect += 1

    def is_collect_all(self):
        if self.now_collect == self.total:
            return True
        else:
            return False

class Treasure(pygame.sprite.Sprite):
    def __init__(self, group, position):
        pygame.sprite.Sprite.__init__(self, group)
        self.width = 32
        self.height = 32
        self.image = create_image_view_data("treasure", position[0], position[1], self.width, self.height)
        # self.image = pygame.Surface([8, 8])
        # self.color = "#E91E63"
        self.rect = pygame.Rect(position[0], position[1], self.width, self.height)
        # self.rect.centerx = random.randint(0, 800)
        # self.rect.centery = random.randint(0, 600)
        # self.rect.centerx = position[0]
        # self.rect.centery = position[1]
        # self.angle = 0

    def collide_player(self, player):
        pass

    @property
    def render_data(self):
        return self.image
        
    @property
    def info(self):
        return {
            "type": str(ObjectType.TREASURE),
            "x": self.rect.x,
            "y": self.rect.y,
            "width": self.rect.width,
            "height": self.rect.height,
            "status": 0
        }