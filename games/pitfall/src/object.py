import pygame.sprite
import pygame
import sys

from mlgame.utils.enum import StringEnum, auto
from mlgame.view.view_model import create_image_view_data

class ObjectType(StringEnum):
    LOG = auto()
    WATER = auto()
    CROCODILE = auto()
    ROLLING_LOG = auto()
    VINE = auto()
    TREASURE = auto()

class Logs(pygame.sprite.Sprite):
    def __init__(self, group, position):
        pygame.sprite.Sprite.__init__(self, group)
        self.width = 48
        self.height = 48
        self.image = create_image_view_data("log", position[0], position[1], self.width, self.height)
        # self.image = pygame.Surface([8, 8])
        # print(self.image)
        # self.color = "#00FFFF"
        self.rect = pygame.Rect(position[0], position[1], self.width, self.height)
        # self.rect = self.image.get_rect()
        # self.rect.centerx = position[0]
        # self.rect.centery = position[1]
        # self.angle = 0

    def collide_player(self, player):
        pass

    @property
    def render_data(self):
       
        return self.image

    @property
    def info(self):
        return {
            "type": str(ObjectType.LOG),
            "x": self.rect.x,
            "y": self.rect.y,
            "width": self.rect.width,
            "height": self.rect.height,
            "status": 0
        }

class Rolling_Logs(pygame.sprite.Sprite):
    def __init__(self, group, position):
        pygame.sprite.Sprite.__init__(self, group)
        self.image = pygame.Surface([40, 40])
        self.color = "#9900FF"
        self.rect = self.image.get_rect()
        self.rect.centerx = position[0]
        self.rect.centery = position[1]
        self.angle = 0

    def update(self):
        if (self.rect.centerx + 30) >= 0:
            self.rect.centerx -= 10
        # else:
        #     self.rect.centerx = -30

    def collide_player(self, player):
        pass

    @property
    def render_data(self):
        return {"type": "image",
                "image_id": 'log1',
                "x": self.rect.x,
                "y": self.rect.y,
                "angle": 0,
                "width": self.rect.width,
                "height": self.rect.height
                }

    @property
    def info(self):
        return {
            "type": str(ObjectType.ROLLING_LOG),
            "x": self.rect.x,
            "y": self.rect.y,
            "width": self.rect.width,
            "height": self.rect.height,
            "status": 0
        }

class Cocodiles(pygame.sprite.Sprite):
    def __init__(self, group, position):
        pygame.sprite.Sprite.__init__(self, group)
        self.image = pygame.Surface([64, 64])
        self.image_id = "crocodile_close"
        self.rect = self.image.get_rect()
        self.rect.centerx = position[0]
        self.rect.centery = position[1]
        self.angle = 0
        self.state = 1

    def collide_player(self, player):
        pass
    
    def update(self, state):
        if state == "off":
            self.image_id = "crocodile_close"
            self.state = 0

        elif state == "on":
            self.image_id = "crocodile_open"
            self.state = 1
    
    @property
    def render_data(self):
        return {"type": "image",
                "image_id": self.image_id,
                "x": self.rect.x,
                "y": self.rect.y,
                "angle": 0,
                "width": self.rect.width,
                "height": self.rect.height
                }
        

    @property
    def info(self):
        return {
            "type": str(ObjectType.CROCODILE),
            "x": self.rect.x,
            "y": self.rect.y,
            "width": self.rect.width,
            "height": self.rect.height,
            "status": self.state
        }