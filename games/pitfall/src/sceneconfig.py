sceneConfig = [{
    "object_list":[
        {
            "type":"treasure",
            "position": [700, 450],
        },{
            "type":"object_1",
            "position": [300, 450],
        },{
            "type":"object_3",
            "position": [500, 450],
        }
    ],
    "isLine": True,
    "isWater": False,
},{
    "object_list":[
        {
            "type":"treasure",
            "position": [700, 450],
        },{
            "type":"object_1",
            "position": [300, 450],
        },{
            "type":"object_1",
            "position": [500, 450],
        }
    ],
    "isLine": False,
    "isWater": False,
},{
    "object_list":[
        {
            "type":"treasure",
            "position": [700, 450],
        },{
            "type":"object_2",
            "position": [550, 430],
        },{
            "type":"object_2",
            "position": [800, 430],
        }
    ],
    "isLine": False,
    "isWater": False,
},{
    "object_list":[
        {
            "type":"treasure",
            "position": [700, 450],
        },{
            "type":"object_1",
            "position": [300, 450],
        },{
            "type":"object_2",
            "position": [800, 430],
        }
    ],
    "isLine": False,
    "isWater": False,
},{
    "object_list":[
        {
            "type":"treasure",
            "position": [700, 450],
        },{
            "type":"object_3",
            "position": [300, 450],
        },{
            "type":"object_3",
            "position": [500, 450],
        }
    ],
    "isLine": True,
    "isWater": True,
}]