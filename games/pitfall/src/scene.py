import pygame
import time

from .treasure import Treasure
from .sceneconfig import sceneConfig
from .object import Logs, Rolling_Logs, Cocodiles
from .line import Line
from .water import Water

class SceneManager:

    def __init__(self, game):
        self.scenes = []
        
        for config in sceneConfig:
            self.scenes.append(Scene(game, config))
        
        self.sceneMaxnum = len(self.scenes)
        # for _ in range(self.scene_num):
            
        self.now_idx = 0

    def change_level(self, go_right=True):
        """
        change level
        """
        if go_right and self.now_idx < self.sceneMaxnum - 1:
            self.now_idx += 1
            return True
        elif go_right == False and self.now_idx > 0:
            self.now_idx -= 1
            return True
        
        return False

    @property
    def level(self):
        return self.now_idx + 1     

    @property
    def render_datas(self):
        return self.scenes[self.now_idx].render_datas

    @property
    def scene(self):
        return self.scenes[self.now_idx]

class Scene:
    """
    Single scene
    """
    def __init__(self, game, settings):
        self._begin_time = time.time()
        self._line_time = 0
        self.line = None
        self.game = game
        self.sprite_group = pygame.sprite.Group()
        self.sprite_group2 = pygame.sprite.Group()
        self.sprite_group3 = pygame.sprite.Group()
        self.sprite_group4 = pygame.sprite.Group()
        self.sprite_group5 = pygame.sprite.Group()
        self.sprite_group6 = pygame.sprite.Group()
        self.cocodiles_collect = True

        for object_data in settings["object_list"]:
            if object_data["type"] == "treasure":
                treasure = Treasure(self.sprite_group, object_data["position"])

            if object_data["type"] == "object_1":
                logs = Logs(self.sprite_group2, object_data["position"])
                
            if object_data["type"] == "object_2":
                rolling_logs = Rolling_Logs(self.sprite_group3, object_data["position"])

            if object_data["type"] == "object_3":
                cocodiles = Cocodiles(self.sprite_group4, object_data["position"])

        if settings["isLine"]:
            self.line = Line(self.sprite_group5)
        
        if settings["isWater"]:
            Water(self.sprite_group6)
            # water = Water(self.sprite_group6)

    def update(self):
        # update 滾筒、藤蔓之類的...
        self.sprite_group3.update()
        t = int(time.time() - self._begin_time)
        line_time = int(100* (time.time() - self._begin_time))

        if self.game.player.attchColdDown > 0:
            self.game.player.attchColdDown -= 0.01

        if t % 6 == 0:
            self.sprite_group4.update("on")
            self.cocodiles_collect = True

        elif t % 3 == 0:
            self.sprite_group4.update("off")
            self.cocodiles_collect = False
        
        if line_time - self._line_time > 30:
            self._line_time = line_time
            self.sprite_group5.update()

        # check collide, or use mlgame.gamedev.physics?
        hits = pygame.sprite.spritecollide(self.game.player, self.sprite_group, True, pygame.sprite.collide_rect_ratio(0.8))
        if hits:
            self.game.treasureManager.collect()
        
        hits2 = pygame.sprite.spritecollide(self.game.player, self.sprite_group2, False, pygame.sprite.collide_rect_ratio(0.8))
        if hits2:
            self.game.player.Death()
        
        hits3 = pygame.sprite.spritecollide(self.game.player, self.sprite_group3, False, pygame.sprite.collide_rect_ratio(0.8))
        if hits3:
            self.game.player.Death()

        hits4 = pygame.sprite.spritecollide(self.game.player, self.sprite_group4, False, pygame.sprite.collide_rect_ratio(0.8))
        if hits4:
            if self.cocodiles_collect:
                self.game.player.Death()
    
        hitWater = pygame.sprite.spritecollide(self.game.player, self.sprite_group6, False, pygame.sprite.collide_rect_ratio(0.8))
        if hitWater:
            self.game.player.Death()

        bound = 25
        if self.line:
            if self.line.data[2] - bound <= self.game.player.rect.centerx <= self.line.data[2] + bound and self.line.data[3] - bound <= self.game.player.rect.centery <= self.line.data[3] + bound:
                self.game.player.AttachLine()

        if self.game.player.isAttachLine:
            self.game.player.rect.centerx = self.line.data[2]
            self.game.player.rect.centery = self.line.data[3]

    @property
    def render_datas(self):
        objects_data = []
        for treasure in self.sprite_group:
            objects_data.append(treasure.render_data)
        for object in self.sprite_group2:
            objects_data.append(object.render_data)
        for object in self.sprite_group3:
            objects_data.append(object.render_data)
        for object in self.sprite_group4:
            objects_data.append(object.render_data)
        for object in self.sprite_group5:
            objects_data.append(object.render_data)
        for object in self.sprite_group6:
            objects_data.append(object.render_data)
        return objects_data

    @property
    def objects_info(self):
        objects_info = []
        for treasure in self.sprite_group:
            objects_info.append(treasure.info)
        for object in self.sprite_group2:
            objects_info.append(object.info)
        for object in self.sprite_group3:
            objects_info.append(object.info)
        for object in self.sprite_group4:
            objects_info.append(object.info)
        for object in self.sprite_group5:
            objects_info.append(object.info)
        for object in self.sprite_group6:
            objects_info.append(object.info)
        return objects_info