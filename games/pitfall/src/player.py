import pygame.sprite

# player state
from mlgame.utils.enum import StringEnum, auto
from mlgame.view.view_model import create_image_view_data


class PlayerAction(StringEnum):
    IDLE = auto()
    MOVE = auto()
    JUMP = auto()
    FALLING = auto()
    AttachLine = auto()
    AttachFALLING = auto()

class Player(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.width = 48
        self.height = 48
        self.walkRight = [create_image_view_data("player_R1", 0, 0, self.width, self.height), create_image_view_data(
            "player_R2", 0, 0, self.width, self.height), create_image_view_data("player_R3", 0, 0, self.width, self.height)]
        self.walkLeft = [create_image_view_data("player_L1", 0, 0, self.width, self.height), create_image_view_data(
            "player_L2", 0, 0, self.width, self.height), create_image_view_data("player_L3", 0, 0, self.width, self.height)]

        # self.image = pygame.Surface([50, 50])
        self.color = "#FFEB3B"
        self.plane_height = 450
        self.rect = pygame.Rect(30, self.plane_height, self.width, self.height)
        
        self.rect.center = (30, self.plane_height)

        # Jump parameter
        self.state = PlayerAction.MOVE
        self.height = 0.0  # the height while agent is jumping
        self.jumpHeight = 120
        self.isAttachLine = False
        self.attchColdDown = 0

        self.isMoving = False
        self.direction = 0  # 0 right / 1 left
        self.anim_index = 0
    def update(self, motions):       
        #self.state = PlayerAction.IDLE
        if(not motions):
            self.isMoving = False
        for motion in motions:
            if(motion == "SPACE") and (self.isAttachLine):
                self.isAttachLine = False
                self.state = PlayerAction.AttachFALLING
            if (motion == "SPACE") and (self.state == PlayerAction.MOVE or self.state == PlayerAction.IDLE):
                self.height = self.rect.centery
                self.state = PlayerAction.JUMP
            elif motion == "LEFT":
                #self.state = PlayerAction.MOVE
                self.isMoving = True
                self.direction = 1
                self.anim_index = (self.anim_index + 1) % 3
                self.rect.centerx -= 10.5
            elif motion == "RIGHT":
                #self.state = PlayerAction.MOVE
                self.isMoving = True
                self.direction = 0
                self.anim_index = (self.anim_index + 1) % 3
                self.rect.centerx += 10.5
            
        
        #Jump and Gravity
        if(self.state == PlayerAction.JUMP):
            self.rect.centery -= 30
            # React Maximum Height
            if(self.rect.centery < self.height - self.jumpHeight):
                self.state = PlayerAction.FALLING
        if(self.state == PlayerAction.FALLING or self.state == PlayerAction.AttachFALLING):
            self.rect.centery += 20
        if(self.rect.centery >= 450):
            self.rect.centery = 450
            self.state = PlayerAction.MOVE
        #Preventing Out of Boundary
        if(self.rect.centerx < 25):
            self.rect.centerx = 25
        if(self.rect.centerx > 775):
            self.rect.centerx = 775
        #print(self.state)

    def AttachLine(self):
        if self.state == PlayerAction.JUMP or self.state == PlayerAction.FALLING:
            self.isAttachLine = True
            self.state = PlayerAction.AttachLine
            self.anim_index = 0

    def Death(self):
        # Do whatever you want when agent is dead
        self.rect.center = (30, self.plane_height)  # Go back to init position

    @property
    def x(self):
        return self.rect.x

    @property
    def y(self):
        return self.rect.y

    @property
    def render_data(self):

        image = None
        if self.isMoving and self.state != PlayerAction.AttachLine:
            if self.direction == 0:
                image = self.walkRight[self.anim_index]
            else:
                image = self.walkLeft[self.anim_index]
        else:
            if self.direction == 0:
                image = self.walkRight[0]
            else:
                image = self.walkLeft[0]

        return {"type": "image",
                "name": "ball",
                "x": self.rect.x,
                "y": self.rect.y,
                "angle": 0,
                "width": self.rect.width,
                "height": self.rect.height,
                "image_id": image["image_id"]
                }
