from .src.game import PitFall

GAME_VERSION = "0.1"
GAME_PARAMS = {
    "()": {
        "prog": "easy-game",
        "game_usage": "%(prog)s <difficulty>"
    },
    "difficulty": {
        "choices": ("EASY", "NORMAL"),
        "metavar": "difficulty",
        "help": "Specify the game style. Choices: %(choices)s"
    },
}

# will be equal to config. GAME_SETUP["ml_clients"][0]["name"]

GAME_SETUP = {
    "game": PitFall,
    "ml_clients": PitFall.ai_clients(),
    # "dynamic_ml_clients":True
}